package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {
	/*
	 * @author - Kulvir Kaur Dhadda Student id - 991539865
	 */

	// Email should have one and only one @ symbol.
	@Test
	public void isValidEmailSymbRegular() {
		String email = "kulvir@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailSymbolExecption() {
		String email = "kulvirgmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	@Test
	public void isValidEmailSymbolBoundaryOut() {
		String email = "kulvir@@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	// Account name should have at least 3 alpha-characters in lowercase (must not
	// start with a number)
	@Test
	public void isValidEmailAccountNameRegular() {
		String email = "kulvir@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailAccountNameUpperCaseException() {
		String email = "Kulvir@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	@Test
	public void isValidEmailAccountNameBoundaryIn() {
		String email = "kul@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailAccountNameBoundaryOut() {
		String email = "ka@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	// Extension name should have at least 2 alpha-characters
	@Test
	public void isValidEmailExtensionRegular() {
		String email = "kulvir@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailBoundaryExtensionException() {
		String email = "kulvir@gmail.co6";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	@Test
	public void isValidEmailExtensionBoundaryIn() {
		String email = "kulvir@gmail.co";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailExtensionBoundaryOut() {
		String email = "kulvir@gmail.c";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	// Domain name should have at least 3 alpha-characters in lowercase or numbers
	@Test
	public void isValidEmailDomainRegular() {
		String email = "kulvir@gma5.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailDomainException() {
		String email = "kulvir@gMa5.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

	@Test
	public void isValidEmailDomainBoundaryIn() {
		String email = "kulvir@gm6.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid", isValid);
	}

	@Test
	public void isValidEmailDomainBoundaryOut() {
		String email = "kulvir@g7.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid", isValid);
	}

}
