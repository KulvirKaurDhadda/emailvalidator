package sheridan;
/* 
 * @author - Kulvir Kaur Dhadda
 * student id - 991539865 */

public class EmailValidator {
	
	
	public static final String REGEX = "^[^0-9A-Z]([a-z0-9]{2,})@{1}([a-z0-9]){3,}.{1}([a-z]{2,})$";
	 
	public static boolean isValidEmail(String email)
	{
		return email.matches(REGEX);
	}

}
